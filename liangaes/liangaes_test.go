package liangaes

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	fmt.Println("begin test")
	m.Run()
	fmt.Println("end test")
}

func testEncryptDecrypt(t *testing.T, origData, key []byte) {

	edata, err := AesCbcNoSaltEncrypt(origData, key, key)
	assert.Equal(t, err, nil)
	dedata, err1 := AesCbcNoSaltDecrypt(edata, key, key)
	assert.Equal(t, err1, nil)
	assert.Equal(t, origData, dedata)
}

func testDecrypt(t *testing.T, data, key []byte) {

	dedata, err1 := AesCbcNoSaltDecrypt(data, key, key)
	assert.Equal(t, err1, nil)
	fmt.Println(data, " => ", dedata)
}

func testDecryptNotEqual(t *testing.T, data, key []byte) {

	dedata, err1 := AesCbcNoSaltDecrypt(data, key, key)
	assert.NotEqual(t, err1, nil)
	fmt.Println(data, " => ", dedata)
}

func TestAesCbcNoSaltEncrypt(t *testing.T) {
	fmt.Println("run encrypt test")

	testEncryptDecrypt(t, []byte("abcdefghijklmnopqrstuvwxyz"), []byte("1234567890123456"))
	testEncryptDecrypt(t, []byte("abcd"), []byte("12345678901234561234567890123456"))
	testEncryptDecrypt(t, []byte(""), []byte("1234567890123456"))
}

func TestAesCbcNoSaltDecrypt(t *testing.T) {
	fmt.Println("run decrypt test")

	testDecrypt(t, []byte(""), []byte("1234567890123456"))
	testDecrypt(t, []byte("1234567890123456"), []byte("1234567890123456"))
	testDecryptNotEqual(t, []byte("1234567890123456123456789012345x"), []byte("1234567890123456"))
	testDecryptNotEqual(t, []byte("1234567890123456123456789012345"), []byte("1234567890123456"))
}

func TestPKCS5UnPadding(t *testing.T) {
	fmt.Println("run PKCS5UnPadding test")

	data, err := PKCS5UnPadding([]byte("1234567890123456"), 16)
	assert.NotEqual(t, err, nil)

	data, err = PKCS5UnPadding([]byte(""), 16)
	assert.Equal(t, err, nil)
	assert.Equal(t, len(data), 0)
}
