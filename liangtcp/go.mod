module gitee.com/viplianghl/golib/liangtcp

go 1.19

require (
	gitee.com/viplianghl/golib/liangaes v0.0.0-20230129031544-f1dc56421070
	github.com/stretchr/testify v1.8.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
