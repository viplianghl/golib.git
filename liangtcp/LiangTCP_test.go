package liangtcp

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	fmt.Println("begin test")
	m.Run()
	fmt.Println("end test")
}

func TestBuffer(t *testing.T) {
	fmt.Println("run Buffer test")

	var msgBuffer bytes.Buffer
	var len uint32 = 4
	binary.Write(&msgBuffer, binary.BigEndian, len)
	msgBuffer.WriteString("abcd")

	temphandle(t, &msgBuffer)

	b, err := msgBuffer.ReadByte()
	assert.Equal(t, err, nil)
	assert.Equal(t, b, byte('b'))

	cd := msgBuffer.Next(4)
	assert.Equal(t, cd[0], byte('c'))
	assert.Equal(t, cd[1], byte('d'))
}

func temphandle(t *testing.T, msg *bytes.Buffer) {

	len := binary.BigEndian.Uint32(msg.Bytes())
	assert.Equal(t, len, uint32(4))

	msg.Next(4)
	a, err := msg.ReadByte()
	assert.Equal(t, err, nil)
	assert.Equal(t, a, byte('a'))
}
